from setuptools import setup

setup(name='ascii_tree',
      version='0.1',
      description='create beautiful ascii trees',
      python_requires='>=3'
)
